# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'EmailTemplate.slug'
        db.add_column(u'django_mail_dbtpl_emailtemplate', 'slug',
                      self.gf('django.db.models.fields.SlugField')(default='test', max_length=50),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'EmailTemplate.slug'
        db.delete_column(u'django_mail_dbtpl_emailtemplate', 'slug')


    models = {
        u'django_mail_dbtpl.emailtemplate': {
            'Meta': {'object_name': 'EmailTemplate'},
            'body': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'subject': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['django_mail_dbtpl']